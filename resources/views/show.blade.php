@extends('layouts.app')

@section('content')
    @if (!Auth::guest())
        <a href="/edit/<?php echo $news->id; ?>" class="btn btn-primary">Редактировать</a>
        <form method="post" action="/delete/<?php echo $news->id; ?>">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-danger">Удалить</button>
        </form>
        <br>
        <br>
    @endif
    <table border="1" width="100%">
        <tr><th>Название: <?php echo $news->title; ?></th></tr>
        <tr><td>Описание: <?php echo $news->body; ?></td></tr>
        <tr><td>Дата создание: <?php echo $news->created_at; ?></td></tr>
    </table>
@endsection