@extends('layouts.app')

@section('content')
    <?php foreach($news as $n): ?>
        <table border="1" width="100%">
            <tr><th>Название: <a href="/show/<?php echo $n->id; ?>"><?php echo $n->title; ?></a></th></tr>
            <tr><td>Описание: <?php echo $n->body; ?></td></tr>
            <tr><td>Дата создание: <?php echo $n->created_at; ?></td></tr>
        </table>
        <br>
    <?php endforeach; ?>

    <nav aria-label="...">
        <ul class="pagination">
            <?php for($p = 1; $p <= $countPage; $p++): ?>
                <li class="page-item <?php echo $p == $currentPage ? 'active' : '' ;?>">
                    <a class="page-link" href="?p=<?php echo $p; ?>"><?php echo $p; ?></a>
                </li>
            <?php endfor; ?>
        </ul>
    </nav>
@endsection