@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="/edit">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $news->id; ?>">

        <div class="form-group">
            <label for="title">Название</label>
            <input
                    type="text"
                    id="title"
                    name="title"
                    class="form-control"
                    placeholder="Введите название"
                    value="<?php echo $news->title; ?>"
            >
        </div>
        <div class="form-group">
            <label for="body">Описание</label>
            <textarea type="text" id="body" name="body" class="form-control" placeholder="Введите описание"><?php echo $news->body; ?></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection