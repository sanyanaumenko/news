<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property boolean $is_deleted
 * @property string $created_at
 * @property string $updated_at
 */
class News extends Model
{
    /**
     * @var string
     */
    protected $table = 'news';
}
