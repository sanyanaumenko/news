<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('show/{id}', 'ShowController@get');
Route::get('/', 'ListController@get');
Route::get('/edit/{id}', 'EditController@get');
Route::post('/edit', 'EditController@save');
Route::post('/delete/{id}', 'DeleteController@delete');

Route::auth();
