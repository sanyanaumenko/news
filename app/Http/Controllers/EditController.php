<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests;

class EditController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get($id)
    {
        return view('edit', ['news' => App\News::findOrFail($id, ['id', 'title', 'body', 'created_at'])]);
    }

    /**
     * @param Requests\NewsEdit $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function save(Requests\NewsEdit $request)
    {
        $request->validate();

        if ($request->id) {
            $news = App\News::findOrFail($request->id);
        } else {
            $news = new App\News();
            $news->created_at = time();
        }

        $news->title = $request->title;
        $news->body = $request->body;
        $news->updated_at = \Carbon\Carbon::now();
        $news->save();

        return redirect(sprintf('/show/%d', $news->id));
    }
}
