<?php

namespace App\Http\Controllers;

use App;

class ShowController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get($id)
    {
        return view('show', ['news' => App\News::findOrFail($id, ['id', 'title', 'body', 'created_at'])]);
    }
}
