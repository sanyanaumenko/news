<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class ListController extends Controller
{
    const LIMIT = 30;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get(Request $request)
    {
        $p = $request->input('p', 1);

        $count = App\News::where('is_deleted', false)->count();

        $news = App\News::where('is_deleted', false)
            ->orderBy('created_at', 'desc')
            ->skip($p - 1 * self::LIMIT)
            ->take(self::LIMIT)
            ->get();

        return view(
            'list',
            [
                'news' => $news,
                'countPage' => ceil($count / self::LIMIT),
                'currentPage' => $p,
            ]
        );
    }
}
