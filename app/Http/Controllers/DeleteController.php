<?php

namespace App\Http\Controllers;

use App;

class DeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param int $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $news = App\News::findOrFail($id);
        $news->is_deleted = true;
        $news->updated_at = \Carbon\Carbon::now();
        $news->save();

        return redirect('/');
    }
}
