<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'title' => 'test',
            'body' => 'test test',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        DB::table('news')->insert([
            'title' => 'test 2',
            'body' => 'test2 test2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
